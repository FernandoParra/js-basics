//MAPS en JS
//utiles cuando necesitamos asociar elementos y valores.
//lista ordenada de llaves y valores
//tanto llave como valor pueden ser de cualquier tipo en los Maps de JS.
//a diferencia de objetos, donde el tipo de las propiedades solo puede ser string.

//declaración

let myMap = new Map();

//agregación:

myMap.set("key", "value");
myMap.set("age", 30);

//obtener valores:
myMap.get("key"); //value
myMap.get("age"); //30

//tambien podemos usar objetos como key.

let objeto1 = {};
let objeto2 = {};

myMap.set(objeto1, 10);

myMap.get(objeto1); //10

//obtener la cantidad de elementos:
myMap.size; //impreme cantidad, en este caso: 3

//saber si un elemento está presente en el map, busca por llave:

myMap.has("key"); //true

//remover un par llave-valor, se especifica por la llave:
myMap.delete("age");

//limpiar el map
myMap.clear();

//ITERAR SOBRE MAPS:

let mapita = new Map();
mapita.set(1, "one");
mapita.set(2, "two");
mapita.set(3, "three");

//para iterar solo sobre las llaves:

for (let key of mapita.keys()) {
  console.log(key); // 1 2 3
}

//iterar solo sobre valores:
for (let value of mapita.values()) {
  console.log(value); // one two three
}

//iterar sobre ambos key y value:
for (let entry of mapita.entries()) {
  console.log(`${entry[0]} -> ${entry[1]} `); //vemos que almacena el par key value como un array de 2 elementos con indice 0 y 1.
}

//con destructuring:
for (let [key, value] of mapita.entries()) {
  console.log(key, value);
}
