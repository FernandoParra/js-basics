/*
En JS podemos utilizar dos tipos de FOR para iterar sobre arrays
for in , y for of.
for in nos da un INDICE que por lo general llamamos index, para acceder con el al indice del array.
for of es como el for each de Java, donde recibimos el objeto almacenado en el array directamente y no necesitamos acceder con array[i]
EJEMPLOS:
*/

let colors = ['red','blue','green']

for (let index in colors){
    console.log(colors[index]); //esta es la manera tradicional de acceder a la variable almacenada en el array, con su indice.
}

for (color of colors){
    console.log(color); //tenemos el mismo resultado de arriba sin la necesidad de acceder a un index ya que con OF nos devuelve la variable directamente.
}

//otro ejemplo:

let word = ['W','O','R','D']

for(letter of word){
    console.log(letter);
}
//output:
/**
 * W
 * O
 * R
 * D
 * sin necesidad de utilizar un word[index].
 * 
 * Ademas este for of sirve para utilizarlo en ITERABLES que veremos mas adelante.
 */