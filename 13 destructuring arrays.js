//Destructuring array
/*
Es separar los elementos de un array en elementos individuales
*/

//es un arreglo. se declaran con []
let employee = ["Jose","Ramirez","Hombre"]

//aquí hacemos el destructuring:
let [nombre, apellido, genero] = employee;

//es equivalente a:
/*
let nombre = employee[0];
let apellido = employee[1];
let genero = employee[2];
*/

//despues de destructuring podemos utilizar cada variable individualmente sin problema: ej:
console.log(nombre); //output: Jose

//Si tenemos menos parametros en el array vs en las variables donde queremos almacenarlo y dividirlo,
//se guardarán los que existan en orden, y las variables que no tengan datos en el array quedan undefined.
let numbers = [1,2,3,4]
let [one, two, three, four, five] = numbers;
//   1     2     3      4    undefined.

//si queremos rescatar solo un elemento del array, las comas son necesarias. ej:

let numbers2= [1,2,3,4,5]

let [, , three2] = numbers2;
console.log(three2); //3

//tambien se pueden mezclar con rest operators. ej: let [firstparameter, ...elements], guardará una variable con el primero, y luego un array con todo el resto.
//tambien con default values, se especifican en el lado izquierdo en donde se guardan las variables. ej: 
let [first, second, third=3] = someArray;

