//CLASS BODY AND METHOD DEFINITIONS

//body de clase es todo lo que está entre { }
//body de clase solo puede contener metodos en JS6, y no propiedasdes (como por ejemplo en JAVA)

class ClaseJS {
  //aqui empieza el body
  /* aqui termina */
}

//tres tipos de metodos

//CONSTRUCTOR EN JAVASCRIPT.
//SE UTILIZA PARA CREAR E INICIALIZAR UN OBJETO, CADA CLASE PUEDE TENER SOLO UN CONSTRUCTOR
//utiliza la palabra reservada constructor(parametros, a,b,c etc){body method}

class Person {
  constructor(name) {
    this.name = name;
  }
}

//uso del constructor:
let person = new Person("Fernando");

console.log(person.name); //Fernando

//el metodo constructor es llamado al instanciar la clase.

//STATIC METHOD
//el metodo estatico es un metodo  que se puede utilizar sin instanciar una clase.
//para declarar metodos estaticos usamos la palabra reservada static antes del nombre de la función estatica.

class Calculator {
  constructor(operatorA, operatorB) {
    this.operatorA = operatorA;
    this.operatorB = operatorB;
  }

  static informationAbout() {
    console.log("metodo estatico, esta es una calculadora!");
  }
}
//uso de metodo estatico:
//para utilizarlos no es necesario instanciar la clase, este metodo funciona igual ya que se llama directamente sobre la clase y no sobre su objeto instanciado.
Calculator.informationAbout();
//output: lo que está dentro del metodo.

//PROTOTYOPE METHOD
//es el tipo de metodo clasico. se utiliza sobre una clase ya instanciada y no utiliza ninguna palabra reservada.

class Persona {
  constructor(nombre) {
    this.nombre = nombre;
  }

  saludar() {
    console.log("Hola " + this.nombre);
  }

  saludoPersonalizado(saludo) {
    console.log("Hola " + this.nombre + " " + saludo);
  }
}
//uso de metodo de prototipo.
//se llama sobre una clase instanciada, este tipo de metodos puede o no tener parametros, y de acuerdo a ello es como se llama. ej: saludar no tiene parametros:
let persona = new Persona("Carmelo");
persona.saludar(); //se llama al metodo sin parametros, solo con parentesis vacios. output : Hola Carmelo

persona.saludoPersonalizado("buen día!"); //se llama el metodo con el parametro deseado. pueden ser varios.
