//usaremos creación de un objeto en javascript con {}.

let firstname = 'John'
let lastname = 'Wick'

let person = { //declaración de un objeto person
    firstname, //primer valor es propiedad del objeto, cuando son del mismo nombre que las variables no necesitamos igualarlas, pero si son diferentes, se asigna con : . ej: firstname : primernombre(let previo)
    lastname
}

console.log(person.firstname); //John

//esta abreviación para no asignar los valores directamente se llama object literals.


//aparte:
//metodo que crea un objeto y ademas tiene metodos dentro:

function createPerson(nombre, apellido, nombreCompleto, edad){
    let nombreCompleto = nombre + "" + apellido;
    return {//return con signos es lo que crea el objeto aquí.
        nombre,
        apellido,
        nombreCompleto,//por objects literals no tenemos que indicar doble el valor, creará un objeto con esos valores.
        esMayor(){ //función abreviada dentro de la creación de objeto.
            return edad>18;//retorna un boolean. y el parametro es asignado por esta función.
        }

    } 
}


//en ES6 las propiedades de los objetos pueden tener espacios siempre que se encierren con "".

let nombre = "Josh"

let human = {
    "primer nombre" : nombre,
    "primer apellido" : apellido
}
console.log(human["primer nombre"]);  //output: Josh

