//Sirve para tener strings concatenados de manera abreviada:

let user = "Felipe"
let saludo = "Bienvenido " + "Felipe" + " a este repo";
 
console.log(saludo); // Bienvenido Felipe a este repo

//para abreviar este string saludo podemos usar back tick -> `` 

let saludoAbreviado =  `Bienvenido ${user} a este repo`
//el string se define entre ` ` y no entre " " o ' ' 
//para pasar el o los parametros dentro del string, usamos el nombre de la variable en el siguiente formato:

// ${variable} 

//ademas nos permite usar comilla doble y simple dentro del string
let stringConComillas = `Este string no "necesita" caracter de 'escape' para comillas` 

//y por ultimo, permiten tener saltos de linea dentro del string:
let stringConSaltoLinea = `Primera linea
Segunda linea
tercera
Sin necesidad de caracter de escape alguno.`
//tambien almacena los tabs y espaciados dentro del string.

