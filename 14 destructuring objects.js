//mismos principios que en arrays aplican, solo que debemos utilizar {} y almacenar el valor con el mismo nombre que tienen en el objeto.

let employee = {
    fname : "Jodri",
    lname : "JOJO",
    gender : "Male"
}

let {fname, lname, gender} = employee;

console.log(fname); //Jodri

//Si queremos renombrar los parametros podemos usar alias.

let {fname : f, lname: l, gender: g} = employee;
//pero solo el alias almacenará la propiedad
console.log(f);//Jodri

