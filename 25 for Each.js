//for each es utilizado sobre maps.

let myMap = new Map();
myMap.set("firstname", "Fernando");
myMap.set("lastname", "Olave");

function mapFunction(key, value, callingMap) {
  console.log(key, value);
  console.log(myMap === callingMap);
}

myMap.forEach(mapFunction); //podemos pasar una función al for each

//tambien aplica para los set, de la misma manera, aunque en set el key y value tendrán el mismo valor.

//al usar forEach siempre debemos especificar el valor, indice y array.

//tambien aplica para arrays.
let miArray = [2, 4, 6, 8, 10];
miArray.forEach(function(valor, indice, array) {
  console.log("En el índice " + indice + " hay este valor: " + valor);
});

//resumen for en JS:
//Maneras de iterar sobre Arrays y Objetos en JavaScript

//Método Array#forEach

let miArray = [2, 4, 6, 8, 10];
miArray.forEach(function(valor, indice, array) {
  console.log("En el índice " + indice + " hay este valor: " + valor);
});

//Método Object#keys combinado con Método Array#forEach

let obj = {
  first: "John",
  last: "Doe"
};

// ES6
Object.keys(obj).forEach(key => console.log(key, obj[key]));

// ES5
Object.keys(obj).forEach(function(key) {
  console.log(key, obj[key]);
});

//Bucle for-i

let miArray = [2, 4, 6, 8, 10];
for (var i = 0; i < miArray.length; i += 1) {
  console.log("En el índice '" + i + "' hay este valor: " + miArray[i]);
}

//Bucle for-in

var miArray = [2, 4, 6, 8, 10];
for (var indice in miArray) {
  console.log(
    "En el índice '" + indice + "' hay este valor: " + miArray[indice]
  );
}

var miObjeto = { marca: "audi", edad: 2 };
for (var propiedad in miObjeto) {
  if (miObjeto.hasOwnProperty(propiedad)) {
    console.log(
      "En la propiedad '" +
        propiedad +
        "' hay este valor: " +
        miObjeto[propiedad]
    );
  }
}

//Bucle for-of. puede ser utilizado para cualquier iterable.

var miArray = [2, 4, 6, "hola", "mundo"];
for (var valor of miArray) {
  console.log("Valor: " + valor);
}
