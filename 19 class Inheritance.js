//HERENCIA EN JS
//Se utiliza la palabra reservada extends
//el objetivo de la herencia en js es crear una nueva clase que hereda desde una clase padre.

class Person {
  constructor() {
    console.log("Person constructor");
  }
}

class Employee extends Person {}

let employee = new Employee(); //al ser instanciado un nuevo objeto de una clase inmediatamente se llama a su constructor.
//pero employee no tiene constructor, pero como extiende de Person que si tiene constructor, ejecuta ese constructor.
//output : Person constructor

//  QUE PASA SI QUEREMOS UN CONSTRUCTOR EN LA CLASE QUE HEREDA TAMBIEN?

class Persona {
  constructor() {
    console.log("Es una persona!");
  }
}
/*
class Empleado extends Persona {
  constructor(compania) {
    console.log("trabaja para:" + compania);
  }
}*/

//misma clase con super:

class Empleado extends Persona {
  constructor(compania) {
    super(compania); //este super es palabra reservada, porque el constructor de la clase padre debe ser llamada antes que el constructor de la clase hijo.
    //al usar super aseguramos que al llamar cualquier metodo en la clase padre, el padre ya está setup correctamente. por lo tanto, en este caso super invoca al constructor
    //de la clase padre antes de invocar el body del constructor hijo.
    console.log("trabaja para:" + compania);
  }
}

let empleado = new Empleado("Wallmart");
//esto da el siguiente error:
/*
Must call super constructor in derived class before accessing 'this' or returning from derived constructor
**/
//porque extiende de una clase que ya tiene constructor, por lo tanto utilizamos super.

//metodos normales en herencia.

class Personita {
  constructor() {
    console.log("es una personita");
  }

  getDNA() {
    return "tiene adn humano!";
  }
}

class Jefe extends Personita {
  constructor(nombre) {
    super(nombre); //setup para llamar constructor clase padre.
    console.log("es un jefe llamado : " + nombre);
  }
}

//Jefe no tiene metodo getDNA. pero por extender a Personita podemos hacer esto:

let jefecin = new Jefe("richar");
jefecin.getDNA(); //y funciona correctamente.

//si Jefe tambien tiene metodo getDNA, entonces llamará directamente a ese metodo en vez de llamar al de clase padre, sin necesidad de usar super()

//Si queremos acceder al metodo del padre de todas maneras, usamos : super.getDNA(), y llamará al metodo del "super" que es superclase, que es igual a clase padre.
