//cuando usar let vs const? se recomienda usar const lo mas que se pueda,
 //ya que la mayoria de los valores utilizados se asignan valores al declarar
 //y no se reasignan sus valores.
 //let -> cuando queremos reasignar el valor en algún momento
 //const -> no se espera una reasignación de valor.

 const PI = 3.14
 const MAX_SIZE = 3000
  
 //let

 let a = 10
 let b = 50
 a = a * b
 b = a - b
 