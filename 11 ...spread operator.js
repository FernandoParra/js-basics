//es lo contrario del rest operator.
//en el caso de rest operator que toma un numero variable de parametros y los mete en un solo array,
//el spread operator toma un arreglo y los almacena en variables individuales.

//rest : toma elementos individuales y combina en un array.
//spread : toma array y divide en elementos individuales.

 //SPREAD OPERATOR SE ESPECIFICA DURANTE LA LLAMADA DE LA FUNCIÓN.


 let displayColorsWithMessage = function (message, ...colors){
    console.log(message)
    for (let color in colors) {
        console.log(colors[color])
    }
}
let colorsArray = ['color1','color2','color3']

displayColorsWithMessage(message, ...colorsArray) //aqui es donde estamos usando SPREAD operator.
