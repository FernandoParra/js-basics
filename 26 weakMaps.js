//en JS6 hay WeakMaps en donde las llaves solo pueden ser objetos, y las referencias a objetos son weaks, lo que significa que no interfieren con el colector de basura.
let myMap = new WeakMap();
let obj1 = {};
myMap.set(obj1, "hello");

console.log(myMap.get(obj1)); //hello

obj1 = null; //ahora la referencia es nula y lo anterior podría fallar.

//no hay muchos usos practicos de weakmap.
