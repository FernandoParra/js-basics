//CLASES EN JAVASCRIPT
//declaracion de una clase en js, se utiliza la palabra class en minuscula, el nombre de la clase, y se especifica que es de tipo clase al tener { }.

class Person {}

//creación de un objeto de esa clase, se puede utilizar let, var, const, etc. luego el nombre que asignamos, y después se iguala a new ClassName();
let person = new Person();

console.log(typeof Person); //function, esto quiere decir que en javascript las clases son funciones al igual que otras, solo que no soy hoisted, es decir, deben
//ser declaradas en un orden especifico para que funcionen correctamente. ejemplo una función declarada en linea 10 puede ser llamada en linea 1, y funciona. una clase no.

//metodos en clases:

//añadir un metodo a una clase en JS es lo mismo que añadir un metodo en Prototype.
class Persona {
  greet() {}
}

let persona = new Persona();
persona.greet(); //aquí llamamos la función de la clase persona.
//clases en JS no son un nuevo tipo de clases ni nada, solo agregan a prototype.
