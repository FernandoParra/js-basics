//palabra reservada LET:
//declaraciones con let tienen un 'scope' por función o por {}. No se pueden declarar en 'desorden' como var.
//si se declara un let dentro de {}, no podrá ser accedido desde fuera de ese parentesis, pero si dentro de otros anindados. ejemplo:

function saludar (nombre) {
    let saludo;
    if(nombre == "Fernando"){
        saludo = "Hola Fernando" //Si funciona ya que saludo se declaró en un parentesis externo.
    }
    console.log(despedida)
}
saludar("Fernando") //output : "Hola Fernando"
