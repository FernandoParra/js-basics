//this hace referencia a las propiedades definidas dentro de la misma función / objeto.
//empleado tiene id y con this podemos acceder a el.
//si corremos la función sin this, no puede acceder a la propiedad.
var empleado = {
    id : 1,
    saludo : function(){ console.log(this.id)}
}

empleado.saludo();

//si tenemos otra función dentro, this.id no obtiene el valor, ya que con this
//hace referencia a las propiedades que tiene en el scope de esa misma función

var empleado2 = {
    id : 3,
    saludo : function(){
        setTimeout(
            function(){
                console.log(this.id)
            },
            1000
        )
    }
}

empleado2.saludo() // undefined. porque this no puede acceder al id superior, solo a las variables dentro de esa misma función.

//para poder acceder a una variable superior, se debe asignar el this a una variable:

var empleado3 = {
    id : 4,
    saludo : function(){
        var self = this
        setTimeout(function(){
            console.log(self.id)
        }
        , 1000)
    }
}
//Self almacena los datos de this anterior, así podemos acceder a sus valores desde adentro de la función.

//otra solución es utilizar funciones flecha, con ello se obtiene el comportamiento esperado sin utilizar 'self'

var empleado4 = {
    id = 5,
    saludo : function(){
        setTimeout(
            () => console.log(this.id)
            ,1000)
    }
}

//esto funciona sin problema al usar función flecha.

//MAS
//this permite decidir que 'foco' usaremos en un objeto.
//al usar this tenemos que pensar:

//implicit binding:
//regla: Al llamar la función, lo que está a la izquierda del punto es a lo que referencia el this.
var estudiante = {
    nombre: "Jose",
    edad: 24,
    decirNombre: function(){
        console.log(this.nombre)
    }
}

estudiante.decirNombre()
//a que referencia this dentro de la función?
//podemos saberlo al llamar la función. segun regla 1, es equivalente a :
//this.nombre = estudiante.nombre

//Siempre se ve lo que va a la izquierda del punto para saber a que se esta refiriendo el this.
 //pasos : Ver en donde se invoca la función (en donde se define dentro del codigo)
 //         Luego, ver que está a la izquierda del punto. a eso es lo que referencia.

 var Person = function(name, age){
    return {
        name : name,
        age : age,
        sayName : function(){
            console.log(this.name)
        },
        mother : {
            name : 'Stacy',
            age : 50,
            sayName : function(){
                console.log(this.name)
            }
        }
    }
 }

 var Jim = Person('Jimmy', 25) //creamos una persona, variable Jim, con nombre y edad.

 Jim.sayName() // hace referencia a la persona Jim, output: Jimmy
 Jim.mother.sayName() // hace referencia a la madre, output: Stacy



 //this también se usa con : call, apply, bind. buscar para mas detalles

 //NEW binding:
 var Animal = function(color, name, type){
    this.color = color,
    this.name = name,
    this.type = type
 }
 //cuando invocamos una función usando NEW, THIS está amarrado al nuevo objeto que estamos creando.
 var zebra = new Animal('black', 'Zebraski', 'Zebra')
  

 //Window binding:
 //por default, this hace referencia al objeto windows. ejemplo:
 var sayAge = function(){
    console.log(this.age)
 }

 var me  = {
     age : 25
 }

 sayAge() //undefined //llamar sin dar contexto, entocnes this llamará al objeto windows.
window.age = 45
sayAge() // 45. ya que this referencia al objeto window por defecto.