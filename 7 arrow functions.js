//funciones flecha

//funcion normal:
var obtenerValor = function (){
    return 10
}
console.log(obtenerValor()) //output : 10 , si se llama sin parentesis muestra el contenido.

//funcion flecha
//se puede omitir la palabra reservada function, reemplazando solo por ()
//para indicar que es función flecha, usamos => 

const getArrowValueV1 = () => {return 10}
console.log(getArrowValueV1)

//si la función flecha solo tiene una declaración en su cuerpo, se puede omitir return y también {}
const getArrowValueV2 = () => 10

console.log(getArrowValueV2()) // 10, funciona igual que las anteriores.

//Función flecha con un parametro, se puede omitir el parentesis:

const getArrowWithParameter = m => m*10

console.log(getArrowWithParameter(5)) // 50

//función flecha con varios parametros, debe ir parentesis

const getArrowWithParameters = (m,n) => m*n/10

//las funciones flechas son solo funciones igual que las demas, escritas de manera abreviada.


//ejemplos
function sum (a , b) {
    return a + b
}
//función flecha se debe almacenar en una variable
let arrowSum = (a, b) => a + b 

//
function isPositive (number) {
    return number >= 0
}
//cuando son funciones de un solo parametro no es necesario que tenga parentesis
let arrowIsPositive = number => number >= 0

//
function randomNumber(){
    return Math.random
}

let arrowRandomNumber = () => Math.random


//
document.addEventListener('click', function(){
    console.log('Click')
})

document.addEventListener('click',()=> console.log('Click'))

//su verdadera importancia es que redefinen THIS dentro de las funciones flecha, diferente a las funciones tradicionales.

class Person {
    constructor(name){
        this.name=name
    }

    printNameArrow(){
        setTimeout(
            ()=>console.log('Arrow: ' + this.name)
            ,100)
    }

    printNameFunction(){
        setTimeout(
            function(){
                console.log('Function: ' + this.name)
            }
            ,100)
    }
}

let person = new Person('Bob')
person.printNameArrow() // Bob, en este caso, con función fecha, this no es redefinido para el nuevo contexto dentro de la función, si no que continua siendo el mismo del objeto. 
person.printNameFunction // vacio. este this hace la misma referencia que al llamar console.log(this.name), debido a que la función interna no tiene la propiedad name.
 