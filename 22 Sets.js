/*
Estructura de datos que contiene una lista de valores unicos, introducidos en ES6.
*/

//declaración:
let mySet = new Set();

//agregación: soporta cualquier tipo de valor
mySet.add("first element of set");
mySet.add(45);
console.log(mySet.size); //2 .. size no lleva parentesis como en java.

mySet.add(45);
console.log(mySet.size); //aun será 2, ya que ignorará la insersión del elemento repetido.

//objetos en set:
let objeto1 = {};
let objeto2 = {};
//objeto1 aparantemente igual a objeto2.
mySet.add(objeto1);
mySet.add(objeto2);
console.log(mySet.size); //4, inserta ambos elementos
//¿porque si no debería soportar elementos iguales? los objetos son unicos porque no son transformados a string, si no almacenados como objetos que tienen distinto espacio de memoria.

//Set con valores iniciales:

let otherSet = new Set([1, 2, "hi", 3, 4, 4]); //4 se repite en su declaración, aun así el set no lo contendrá repetido.
otherSet.size; //5, sin contar el numero repetido.

//tambien se pueden agregar elementos de manera encadenada
let chainSet = new Set()
  .add(4)
  .add("hello")
  .add(123);

//BUSCAR ELEMENTOS DENTRO DE UN SET.
//mySet contiene el elemento '1'?
mySet.has(1); //false
mySet.has(45); //true

//REMOVER ELEMENTOS DE SET.
//quitar 45.

mySet.delete(45);

mySet.has(45); //false

//OTROS METODOS DE SET:

//FOR EACH, aplica una función a cada elemento del set.
let thisSet = new Set([1, 2, 3, "Hello world", "OHHOH!"]);

thisSet.forEach(elemento => {
  console.log(elemento);
});

//CLEAR, elimina todos los elementos del set. es una función por lo que se llama con ().

thisSet.size; //5
thisSet.clear();
thisSet.size; //0

//ENTRIES: devuelve un array con los elementos de un set y nos permite utilizarlo como tal.

let iterableSet = new Set([1, 2, 3, 4, 5, 7]);

for (let value of iterableSet.entries()) {
  console.log(value);
}

//VALUES: devuelve iterable con los elementos del set, directamente, no como array.

for (let value of iterableSet.values()) {
  console.log(value);
}

//KEYS: hace lo mismo que values()

for (let value of iterableSet.keys()) {
  console.log(value);
}

//transformación a ARRAY:

let myArray = Array.from(iterableSet);

//SET NO TIENE LAS FUNCIONES filter() y map() que ARRAY si tiene.
