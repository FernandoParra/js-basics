//podemos usarlo cuando no sabemos la cantidad de parametros que ingresarán a la función, ej:
//tenemos función que imprime colores, pero no sabemos la cantidad de inputs que tendrá

 let displayColors = function(){ //no declara recibir parametros
    for (let i in arguments) { //por cada elemento i en el arreglo de arguments
        console.log(arguments[i]) //imprime ese argumento
    }
 }

 displayColors('rojo')
 displayColors('rojo','verde','azul') 
 //ambos output son impresos con la función.

 //parte 2. ahora queremos imprimir un mensaje antes de mostrar los colores, pero si lo hacemos pasando el parametro
 //antes del for, se imprimirá duplicado ya que también forma parte de los argumentos

 //REST parameter representa un numero indefinido de argumentos como un array.
 //con ello podemos aclararle a la función que queremos el mensaje aparte, y luego la lista indefinida de argumentos
 //utiliza ... antes del nombre que queramos darle a este arreglo indefinido de parametros que vamos a recibir.

 let displayColorsWithMessage = function (message, ...colors){
     console.log(message)
     for (let color in colors) {
         console.log(colors[color])
     }
 }

 let messageToFunction = 'Printing colors:' //definimos un mensaje para la función.
 displayColorsWithMessage(messageToFunction, 'Rojo', 'Verde','Cafe','Negro') //pasamos el mensaje que es un parametro ya definido, y luego un numero indefinido de parametros que serán recibidos por el rest parameter almacenandolos en un array.
 //output:
 /**
  Printing colors:
  Rojo
  Verde
  Cafe
  Negro
  */


  //RESUMEN: REST PARAMETER ES USADO PARA REPRESENTAR UN NUMERO VARIABLE DE PARAMETROS (ARGUMENTOS) QUE PODEMOS PASAR A UNA FUNCIÓN,
  //Y SU SINTAXIS SON ... ANTES DEL NOMBRE DEL ARRAY VARIABLE QUE SE PUEDE UTILIZAR DENTRO DE LA FUNCIÓN.
  //EL REST OPERATOR ESPERA QUE LE PASEMOS ELEMENTOS INDIVIDUALES A LA FUNCIÓN, NO FUNCIONA SI PASAMOS UN ARREGLO.
  // REST OPERATOR SE ESPECIFICA DURANTE LA DECLARACIÓN DE LA FUNCIÓN
