//var tiene 'functional scope'
//let tiene 'block scope'
var a = 1
var b = 2
if(a === 1) { // === compara que sea mismo valor y mismo tipo. == compara mismo valor sin importar tipo.
    var a = 10 //a ahora almacena el valor 10.
    let b = 20 //solo dentro de este bloque if, b = 20.
    console.log(a) // 10
    console.log(b) // 20
}
console.log(a) // 10 //al salir del bloque, a continua siendo 10 ya que lo reasignamos como 'var'
console.log(b) // 2 //al salir del bloque, b es 2 ya que fue asignado como 20 solo dentro del bloque.

//otra diferencia es que let no puede ser re-declarada:
var c = 1
var c = 2
console.log(c) // output: 2
let d = 1
let d = 2 //Identifier 'd' has already been declared
console.log(d) // output: 1