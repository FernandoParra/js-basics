//en ES5 solo existía la collection type de array, y para estructuras de datos mas complejas había que hacerlo manualmente. o utilizar objetos.
//conceptos:
/*
Sets :
 Lista de valores que no puede contener duplicados.  no podemos acceder a los valores individuales, solo podemos checkear que un valor está presente en el set o no.
 (en el array si podemos acceder a un elemento individual, ejemplo array[index], en sets no.)

 Maps: 
 Es una collection de key value pairs.
 es decir, es una colección de elementos agrupados en pares, por una llave y un valor asociado a esa llave unica.
  
*/
