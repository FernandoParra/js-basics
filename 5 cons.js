//constantes de solo lectura
//al igual que let, son 'block scope' y no pueden declararse 'desordenadas'
//a diferencia de let, se debe 'setear' su valor al declararse, y este valor no puede cambiar en la ejecución del programa.

let num1 //funciona
const num2; // no funciona
const num3 = 10 //funciona

//objetos:
const objeto1 = { nombre: "Joselito"}

console.log(objeto1.nombre) //Joselito

//reasignar valor a objeto 1 no funciona
objeto1 = {} //no funcionara
//si funciona reasignar valores a las propiedades del objeto
objeto1.nombre = "Juanin"
console.log(objeto1.nombre) //Juanin
