//cuando un parametro no es especificado durante el llamado de una función, ese parametro tomará
//el valor por defecto si es que la función lo define así en su definición, si no, será UNDEFINED:
 
let getValue = function(value){
    console.log(value) //output: undefined
}

//default value for parameter:
let getValueWithDefault = function(value=10){
    console.log(value)
}
//call with: getValueWithDefault(5) -> output : 5
//call with: getValueWithDefault() -> output : 10


//aplica a funciones con varios parametros, en ese caso, si alguno de los parametros no se pasa, se define como undefined al llamar la función.
let getMoreParameters = (value=10, bonus=5) =>{
    console.log(value + bonus)
}

//getMoreParameters() -> 15
//getMoreParameters(20,10) -> 30
//getMoreParameters(undefined,5) -> 5+5 = 10
//getMoreParameters(5) -> 10 -> para ignorar el segundo no es necesario declararlo undefined.

//valores por defecto pueden ser mas complejos y funcionarán igual, o depender unos de otros.
//ej : (value=10, bonus=value*5)


