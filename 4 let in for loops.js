//imprimir 5 numeros usando var:
for(var i = 0; i<5 ; i++){
    console.log(i) // 0 1 2 3 4  
}

//si queremos utilizar el valor en una función hay un error ya que var solo pasará la referencia al valor:
for(var i = 0; i<5 ; i++){
    setTimeout(function(){console.log(i)},1000) //output: 5 5 5 5 5
}

//pero al usar let, esto funciona correctamente ya que se mantiene en el bloque:
for(let i = 0; i<5 ; i++){
    setTimeout(function(){console.log(i)},1000) //output: 0 1 2 3 4 
}