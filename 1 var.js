//ES6 -> agrega nuevas caracteristicas a JS.
//Variables

//declaración de una función con un parametro nombre
// la declaración de var saludo se puede hacer así, el interprete lo 'arregla' por nosotros. incluso podría declararse al final y funcionaría bien.
//para declarar variables en JS se utiliza var.
function saludar(nombre){
    if(nombre == "Fernando"){
        var saludo = "Hola Fernando"
    }else{
        var saludo = "Hola!"
    }
    console.log(saludo)
}

//llamado de la función, con solo su nombre.
saludar("Fernando") // output: Hola Fernando
saludar("Ignacio") //output: Hola!



