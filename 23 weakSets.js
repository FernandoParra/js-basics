//diferente a 'strong set' ya que solo puede almacenar referencias a objetos y no valores primitivos.
//se puede utilizar cuando queremos que un elemento desaparezca del set cuando se vuelva nulo.
//permite recolector de basura tecnicamente.

//ejemplo con set normal
let mySet = new Set();
let key = {};
mySet.add(key);
mySet.size; //1
key = null;
mySet.size; //1 , aun es 1 pese a que key ahora es null, en algunos casos queremos que al pasar esto, el elemento se remueva del set.

//weakset permite solo referencias a objetos, esas referencias son weak, es decir,  permite garbage collection cuando las referencias ya no almacenan el objeto.
let weakSet = new WeakSet();
let newKey = {};
weakSet.add(newKey);
weakSet.size; //no funciona bien con weakset, es undefined. por lo que weakset se usa solo con has.
weakSet.has(newKey); //true

newKey = null;
weakSet.has(newKey); //debería ser 0 pero dependerá del colector de basura de js.
