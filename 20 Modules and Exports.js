/**
 *
 * MODULES EN JS SIRVEN PARA DIVIDIR LOS PROGRAMAS SEGUN ALGÚN CRITERIO A DEFINIR POR LOS PROGRAMADORES.
 * PERMITEN MANTENER EL FOCO EN ALGUNA TAREA ESPECIFICA.
 * "SEPARATION OF CONCERN"
 * AYUDA A FACILITAR LA BUSQUEDA DE IMPLEMENTACIONES A QUIENES NO ESTEN FAMILIARIZADOS CON EL CODIGO.
 *  NO ESTÁN BUILD IN EN JS.
 *  EN ES6 SE ESTANDARIZÓ EL USO DE LA SINTAXIS PARA MODULOS, ANTERIORMENTE HABÍA QUE UTILIZAR LIBRERIAS EXTERNAS
 * EJEMPLO:
 * MODULO A. necesita función de B, por lo tanto, MODULO A import C
 * MODULO B tiene una función C que A necesita, entonces MODULO B export C
 */

//EXPORTS
//NAMED EXPORTS:
//moduleA.js
//queremos utilizar firstName en otro modulo, para ello se agrega export en la declaración de la variable:
export let firstName = "Dick";

//moduleB.js
import { firstName } from "./moduleB.js"; //se utiliza import, se especifica entre {} que variable queremos importar, y se espefica el directorio del archivo con ./ o ../../ etc-
console.log(firstName); //Dick, funciona aunque moduleB no tiene ninguna declaración de firstName, porque lo está importando de moduleB.js

//importar multiples variables desde otro modulo:
import { firstName, lastName } from "./moduleB.js";

//si queremos exportar muchas variables no es necesario que c/u tenga export en su declaración, en ese caso:
//moduleA.js:
let fName = "Someone";
let lName = "Something";

export { fName, lName };

//es equivalente a ir uno a uno. este export debe ir AL FINAL del archivo.

//IMPORT CON ALIASES:
//moduleB.js
import { fName as primerNombre, lName as primerApellido } from "./moduleA.js";
//al usar alias, ahora dentro de moduleB tenemos que usarlos como estan nombrados.
console.log(primerNombre); //Someone

console.log(fName); //ERROR

//ademas, las variables importadas no pueden cambiar de valor, por ejemplo:
primerNombre = "Ross"; //ERROR
//no se pueden cambiar valores a variables importadas. imports are read only

//SI SE PUEDEN CAMBIAR propiedades de objetos.
//moduleA.js
let objeto1 = {
  name: "Zoey",
  lastName: "Deschanel"
};
export { objeto1 };

//moduleB.js
import { objeto1 as obj } from "./ModuleA.js";

obj.name = "Ross";
console.log(obj.name); //Ross en vez de Zoey. funciona.

//fin NAMED EXPORTS

//DEFAULT EXPORTS:
//cuando se exporta o importa solo una variable o función usamos la keyword default, y no es necesario usar {}
//moduleA.js
let firstName = "Chanchiwawi";
export default firstName;

//modeleB.js
import primerNombre from "./moduleA.js";
//import no lleva default, y puedes cambiar el nombre como alias sin especificar a que variable lo estás haciendo ya que ese archivo solo exporta uno.
console.log(primerNombre); //Chanchiwawi

//FIN DEFAULT EXPORTS.

//EXPORT FUNCTIONS AND CLASSES
//moduleA.js
export function greet(message) {
  console.log(message);
}

export class Saludador {
  constructor(message) {
    console.log(message);
  }
  saludar() {
    console.log("Holi");
  }
}
//igual que exportar variables individualmente

//moduleB.js
import { greet, Saludador } from "./moduleA.js";
greet("SHAA!");
//funciona, se importa igual que variables, solo se especifica el nombre de la variable sin ().
let saludadorcin = new Saludador("Holis");
saludadorcin.saludar();
